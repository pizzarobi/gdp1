#include <stdio.h>
#include <stdlib.h>

int pascal(int z, int s);

int main(int argc, char * argv[]){
  int x,y;
  
  int hoehe = atoi(argv[1]);
  //printf("%d\n",pascal(atoi(argv[1]),atoi(argv[2])));

  for(x = 0; x < hoehe; x++){
    int z = x;
    
    // Abstandsgenerator
    for(y = hoehe-x ; y > 0; y--)
        printf("  ");
   
    
    // Pascalwerte anzeigen
    for(y = 0; y <= x;y++){
      printf("%4d",pascal(z,y));
      z--; 
    }
    printf("\n");
  }
  return 0;
}

int pascal(int z, int s){
  if (z == 0 || s == 0)
    return 1;
  else
    return pascal(z-1,s) + pascal(z,s-1);
}

