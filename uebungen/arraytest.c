#include <stdio.h>

void printarray(int* test);

int main(){
  int test[4] = {1,2,3,4};
  int *array = test;


  printf("%d\n",*array);
  printf("%d\n",*(array+1));
  printf("%d\n",*(array+2));

  printf("\n");

  char * s = "TestoE";
  size_t len = 0;

  for(int i = 0; s[i] != '\0';i++)
    len++;

  printf("%d\n",len);
  

  return 0;
}

//void printarray(int* test){
  
  //printf("%d",*test);
  //printf("%d",*test+sizeof(int));
  //printf("%d",*test+sizeof(int)*2);
//}
