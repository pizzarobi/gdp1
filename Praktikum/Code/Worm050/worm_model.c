// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm model

#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// ********************************************************************************************
// Global variables
// ********************************************************************************************

enum ColorPairs theworm_wcolor; 


// *****************************************************
// Functions concerning the management of the worm data
// *****************************************************

// START WORM_DETAIL
// The following functions all depend on the model of the worm

// Initialize the worm
extern enum ResCodes initializeWorm(struct worm* aworm, int len_max, struct pos headpos, enum WormHeading dir, enum ColorPairs color){
  // Local variables for loops etc.
  int i;

  // Initialize last usable index to len_max -1
  // theworm_maxindex
  aworm->maxindex = len_max -1;

  // Initialize headindex
  aworm->headindex = 0; //Index pointing to head position is set to 0

  // Mark all elements as unused in the arrays of positions
  // This allows for the effect that the worm appears element by element at the start of each level
  for(i = 0; i <= aworm->headindex; i++)
  {
    aworm->wormpos[i].y  = UNUSED_POS_ELEM;
    aworm->wormpos[i].x  = UNUSED_POS_ELEM;
  }
 
  //Initialize position of worms head
  aworm->wormpos[aworm->headindex] = headpos;

  //Initialize the heading of the worm
  setWormHeading(aworm, dir);

  // Initialize color of the worm
  aworm->wcolor = color;

  return RES_OK;
}

// Show the worms's elements on the display
// Simple version
extern void showWorm(struct worm* aworm) {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed
  placeItem(
      aworm->wormpos[aworm->headindex].y,
      aworm->wormpos[aworm->headindex].x,
      SYMBOL_WORM_INNER_ELEMENT,aworm->wcolor);
}

void cleanWormTail(struct worm* aworm)
{
  int max_len = WORM_LENGTH;

  // compute tailindex
  int tailindex = (aworm->headindex + 1) % max_len;

  // Check the array of worm elements
  // Is the array element at tailindex already in use?
  if(aworm->wormpos[tailindex].y != UNUSED_POS_ELEM)
  {
    //YES: place a SYMBOL_FREE_CELL at the tail's position
    placeItem(
        aworm->wormpos[tailindex].y,
        aworm->wormpos[tailindex].x,
        SYMBOL_FREE_CELL,
        COLP_FREE_CELL);
  }
}

void moveWorm(struct worm* aworm, enum GameStates* agame_state) {
  // Compute and store new head position according to current heading.
  struct pos new_headpos;

  new_headpos.y = aworm->wormpos[aworm->headindex].y + aworm->dy;
  new_headpos.x = aworm->wormpos[aworm->headindex].x + aworm->dx;

  // Check if we would hit something (for good or bad) or are going to leave
  // the display if we move the worm's head according to worm's last
  // direction. We are not allowed to leave the display's window.
  if (new_headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (new_headpos.x > getLastCol() ) { 
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (new_headpos.y < 0) {  
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (new_headpos.y > getLastRow() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.
    // Check if the worm's head will collide with itself at the new position
    if(isInUseByWorm(aworm, new_headpos))
    {
      // No Good Stop game
      *agame_state = WORM_CROSSING;
    }
  }

  // Check if status of *agame_state
  // Go on if nothing bad happened
  if(*agame_state == WORM_GAME_ONGOING)
  {
    // So all is well: we did not hit anything bad and did not leave the
    // window. --> Update the worm structure.
    // Increment theworm_headindex
    // Go round if end of worm is reached (ring buffer)
    aworm->headindex = (aworm->headindex + 1) % WORM_LENGTH;
    // Store new coordinates of head element in the worm structure
    aworm->wormpos[aworm->headindex] = new_headpos;
  }
}

// A simple collision detection
bool isInUseByWorm(struct worm* aworm, struct pos new_headpos)
{
  int i;
  bool collision = false;
  i = aworm->headindex;
  do {
    // Skip check for tail element
   if(i==(aworm->headindex + 1) % WORM_LENGTH)
   {
      i = (i+1) % WORM_LENGTH;
   }

    //Compare the position of the current worm element with the new_headpos
    if(new_headpos.y == aworm->wormpos[i].y && new_headpos.x == aworm->wormpos[i].x){
      collision = true;
    }

    i = (i+1) % WORM_LENGTH;
  } while (i != aworm->headindex && aworm->wormpos[i].x != UNUSED_POS_ELEM);
  // Return what we found
  return collision;
}


// Setters
void setWormHeading(struct worm* aworm, enum WormHeading dir) {
  switch(dir) {
    case WORM_UP :// User wants up
      aworm->dx=0;
      aworm->dy=-1;
      break;
    case WORM_DOWN :// User wants down
      aworm->dx=0;
      aworm->dy=+1;
      break;
    case WORM_LEFT :// User wants left
      aworm->dx=-1;
      aworm->dy=0;
      break;
    case WORM_RIGHT :// User wants right
      aworm->dx=+1;
      aworm->dy=0;
      break;
  }
} 

// Getters
struct pos getWormHeadPos(struct worm* aworm){
  // Structures are passed by value!
  // -> we return a copy here
  return aworm->wormpos[aworm->headindex];
}
